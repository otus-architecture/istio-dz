# Практика к занятию по теме "Service mesh на примере Istio"

## Зависимости

Для выполнения задания вам потребуется установить зависимости:

- [Kubectl 1.25.6](https://github.com/kubernetes/kubernetes/releases/tag/v1.25.6)
- [Istioctl 1.17.1](https://github.com/istio/istio/releases/tag/1.17.1)

## Установка

Для запуска приложения:

```shell script
kubectl apply -f manifests/app.yaml
```

Для настройки istio gateway и правил роутинга:

```shell script
kubectl apply -f manifests/gateway.yaml
```

Для нагрузочного тестирования:

```shell script
kubectl apply -f manifests/load.yaml
```

## Карта сервисов с балансировкой трафика между версиями

![Карта сервисов с балансировкой трафика между версиями](kiali-map.png)

## Удалить ресурсы

```shell script
kubectl delete -f manifests
```
