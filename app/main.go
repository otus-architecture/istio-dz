package main

import (
	"fmt"
	"net"
	"net/http"
	"os"
)

var host string
var port string
var hostname string
var version string

func init() {
	host = os.Getenv("HOST")
	port = os.Getenv("PORT")
	version = os.Getenv("VERSION")

	var err error
	hostname, err = os.Hostname()
	if err != nil {
        fmt.Fprintf(os.Stderr, "error: %v\n", err)
        os.Exit(1)
	}
}

func main() {
	http.HandleFunc("/", getRoot)
	err := http.ListenAndServe(net.JoinHostPort(host, port), nil)

	if err != nil {
        fmt.Fprintf(os.Stderr, "error: %v\n", err)
        os.Exit(1)
	}
}

func getRoot(w http.ResponseWriter, r *http.Request) {
	str := fmt.Sprintf("HOSTNAME=%s, VERSION=%s", hostname, version)
	w.Write([]byte(str));
}
